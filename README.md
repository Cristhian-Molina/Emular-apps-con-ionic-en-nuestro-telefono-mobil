Entorno de trabajo para app moviles con ionic framework y cordova apache en ubuntu
==================================

Instalación de NodeJS
-------------

Para instalar nodeJS debemos ejecutar los siguientes comandos, los cuales añadirán la dependencia de nodejs y posteriormente instalaremos el paquete:

Instalamos curl:
```
sudo apt-get install curl

```
Añadimos el origen de nodejs:
```
curl https://deb.nodesource.com/setup | sudo bash -

```
Actualizamos dependencias:
```
sudo apt-get update

```
Instalamos node:
```
curl -sL https://deb.nodesource.com/setup_0.12 | sudo -E bash -
sudo apt-get install -y nodejs

```
Para comprobarlo, abrimos una terminal y ejecutamos:
```
node -v

```
Instalación de apache-cordova y ionic framework
-------------
Estos dos paquetes de node los instalaremos para poder crear aplicaciones móviles híbridas. Los instalaremos de forma global y con permisos de sudo para tenerlos disponibles en todo el sistema:

```
sudo npm install -g cordova

```
```
sudo npm install -g ionic

```
Instalando Java 8 JDK
-------------
Añadir la webupd8team repositorio PPA Java en su sistema e instalar Oracle Java 8 utilizando siguiente conjunto de comandos.
```
sudo add-apt-repository ppa:webupd8team/java
sudo apt-get update
sudo apt-get install oracle-java8-installer

```
Después de instalar correctamente orácle Java utilizando el paso anterior verificar la versión instalada usando los siguientes comandos.

```
java -version

```

Apache ANT
-------------

Además, debemos de verificar que tenemos apache-ant instalado para poder trabajar con cordova. El comando que lo instala es:
```
sudo apt-get install ant
```

Descargando SDK Android
-------------

ir a la pagina de [android-sdk](https://developer.android.com/sdk/index.html) y bajar hasta la opción que dice **SDK Tools Only**  al final de la pagina y darle click en android-sdk_r24.2-linux.tgz que es la versión hasta el momento, acepten los términos y a descargar

Descomprimiendo archivo descargado
-------------

Una vez descargado su archivo por defecto estará en la carpeta de descargas solo tendrán que ir a el home, osea a la carpeta personal y hay crear una carpeta , yo le coloque el nombre de android, 

```
mkdir android
```
recuerden que hay que crearla entro del directorio personal algo así seria la ruta **/home/user/android/**

y muevan el archivo descargado de descargas a la carpeta android, cuando hayan movido el archivo a la carpeta que crearon con el nombre de android  descomprimen el archivo android-sdk_r24.2-linux.tgz con el siguiente comando 

```
tar xzvf android-sdk_r24.1.2-linux.tgz
```

Creando la ruta PATH para que ejecutar el comando android
-------------

Creamos en nuestro PATH una referencia a Android Manager. para ello editamos con permisos de sudo el fichero .bashrc que esta en nuestra home

```
sudo gedit ~/.bashrc
```

Nos vamos a el final de las lineas y escribimos esta
```
#Android SDK
PATH="$HOME/android/android-sdk-linux/tools:$HOME/android/android-sdklinux/platform-tools:$PATH"
```
guardamos y cerramos el archivo , para luego en la terminal pulsar lo siguiente

```
export PATH="$HOME/android/android-sdk-linux/tools:$HOME/android/android-sdk-linux/platform-tools:$PATH"
```

luego cerramos la sesión de ubuntu para que se efectúen los cambios, 

Abrir el Asistente de Android Manager
-------------
Con nuestra terminal abierta escribimos el comando 
```
android
```

Esto nos deberá de abrir el instalador de paquetes de android SDK, si hay imprime en la terminal que el comando no lo reconoce, debes de ejecutar nuevamente la instrucción de exportar la variable para android
```
export PATH="$HOME/android/android-sdk-linux/tools:$HOME/android/android-sdk-linux/platform-tools:$PATH"
```
no importa de que ubicación estés entro de la terminal deberá de ejecutarse el asistente.

Instalar Paquetes de Android
-------------

De la carpeta que dice tools instalamos todos las dependencias, y la versión de android la que dice android 5.0.1 (api 21) y solo palomeamos las opciones SDK platform , sigue los pasos y acepta los términos y a esperar a que termine tu descarga.


Crear Primer Proyecto con Ionic 
-------------

Independiente en donde vayas a crear tus proyectos para ionic, en mi caso suelo crearlos en /home/user/Documentos/ionic-proyects/
ya habiendo instalado cordova y iononic de forma global como lo hicimos en el anterior paso, ejecutamos entro de esa carpeta o la de tu preferencia el siguiente comando 
```
ionic start mi-primera-app tabs 
```
para entender el parámetro de tabs es recomendable ir a  la guía de [ionic-Start a project](http://ionicframework.com/getting-started/) 

entramos en la carpeta con 
```
cd mi-primera-app 
```
Ahora Vamos agregar la plataforma para poder correr nuestra app de ionic en el teléfono mobil, 
```
ionic platform add android
```
esto nos agregara en el proyecto una carpeta platform en ella una carpeta de android donde tendrá todos los archivos para que se comporte como una app nativa de android.

Configurar para que corra tu app en el teléfono mobil
-------------
Ahora bien ya tenemos agregada la plataforma de android pero debemos especificar ciertos parametros en 2 archivos que están entro de esa carpeta de platform/android.
Hay buscamos el primer archivo para modificar que seria **build.gradle**  y buscamos el objecto android y entro especificamos otros objectos
```
android{
	 buildTypes {
        debug {
            debuggable true
        }
     }
}
```
Guardamos los cambios y buscamos el siguiente archivo a modificar **AndroidManifest.xml**, abrimos el archivo y en la etiqueta de  application agregamos el siguiente atributo
```
<application android:debuggable="true">
```
Lo siguiente seria configurar tu teléfono mobil para que acepte instalar  fuentes desconocidas, y depuración por usb, esto es independiente del teléfono a veces la configuracion puede cambiar dependiendo de cada terminal mobil.

Una vez configurado y activado estas opciones vamos al terminal de ubuntu y crearemos un archivo de reglas en a siguiente ubicación, en mi caso lo creare con el editor de consola de la terminal que es vim con el siguiente comando

```
sudo vim  /etc/udev/rules.d/51-android.rules
```
para agregar contenido tipeamos la tecla i de nuestro teclado para entrar en modo edición, en mi caso voy a instalar una regla para que reconozca mi teléfono mobil de marca motorola
```
SUBSYSTEM=="usb", ATTR{idVendor}=="22b8", MODE="0666", GROUP="plugdev" 
```
este seria el id que reconoce que es un teléfono motrorola **22b8**
ATTR{idVendor}=="22b8" .
Si no tienes un motorola en este [link](http://developer.android.com/tools/device.html#VendorIds) veras todos los idVendor de las marcas de los telefonos, solo debes copiar el idVendor que sale en la lista y colocarlo como parametro, al haber determinado tu marca de telefono guarda el archivo como yo lo abri con el editor de vim, seria dar la tecla <kbd>ESC</kbd> luego <kbd>:</kbd><kbd>w</kbd><kbd>q</kbd> que traduce en salir de modo edicion, guardar y cerrar los cambios . 

Luego daremos permisos a ese archivo creado con el siguiente comando
```
chmod a+r /etc/udev/rules.d/51-android.rules
```

Correr nuestra app en el teléfono mobil
---------

Una vez hecho todo lo anterior, nos quedaría hacer por ultimo conectar tu teléfono mobil a tu maquina y situarnos en el donde esta el directorio de la app, como ya les había comentado yo en lo personal guardo mis proyectos en /home/user/Documentos/ionic-proyects/mi-primera-app.

ya estando en el directorio de tu app creada en la raíz ejecuta el siguiente comando para que se vea en tu teléfono mobil

```
ionic run android
```

Y si todos los pasos están bien hechos deberás ver en la terminal que se muestran muchos procesos corriendo para que se ejecute en tu teléfono mobil, cuando finalice en tu teléfono se vera la app que has hecho.

Emular la aplicación con Genymotion 
---------

> **Note:**

> - Antes que nada deben instalar virtualbox para hacer uso de esta herramienta. En mi caso lo instale [descargandome](https://www.virtualbox.org/wiki/Linux_Downloads) el paquete de instalación de 32 bits y lo instale con el centro de software de ubuntu dándole doble click en el .deb descargado

Primero que todo hacerse con una cuenta en [genymotion](https://www.genymotion.com/), en las opciones de perfil al crear la cuenta en **Company size: personal use y Usage type: testing** para poder descargar el archivo de instalación, al crear la cuenta te notificaran por correo para que valides la cuenta, una ves hecho ese paso, pasa a descargar el paquete en mi caso descargo la versión de 32 bits para ubuntu.

Instalar Genymotion
---------

Al terminar la instalación abre la consola y nos dirigimos a la carpeta contenedora donde esta el archivo, por defecto en la carpeta descargas


```
cd Descargas
```

luego el siguiente comando que le dará permisos de ejecución para poder instalar
```
sudo chmod +x genymotion-2.4.0_x86.bin
```
aquí el nombre del paquete bin puede variar dependiendo de la versión que este hasta el momento, luego ejecutamos el archivo con el siguiente comando estando entro de la misma carpeta contenedora del archivo .bin como ya estoy en la carpeta descarga solo escribo en consola lo siguiente 

```
./genymotion-2.4.0_x86.bin
```
al ejecutar esa linea de comando lo que te mostrara sera una dirección donde se ira a instalar dale Y para confirmar que si, si no quieres que se instale entro de la carpeta de descargas, solo mueve el archivo .bin a la personal o donde quieras y has lo mismo nueva mente,  al dar que si en el proceso verificara si tienes instalado virtualbox, al terminal la ejecución en la terminal si todo anda bien, abre el administrador de carpetas y entra en la carpeta donde se instalo el genymotion y dale doble click en el ejecutable que tiene como nombre solo Genymotion.

Crear nuestra primero dispositivo
------------------
Si al ejecutar te sale un error  **ERROR: Unable to load vitualbox engine** para solucionarlo sigue estos pasos.
1. Ejecuten VirtualBox
2. Ir a las configuraciones de VirtualBox. (Boton de engranaje)
3. Seleccionar red, en la opción conectado a : cámbialo a la que dice adaptador solo-anfitrión luego dale aceptar.
4. Luego ejecutan Genymotion y ya podrán ingresar con su Usuario y contraseña para conectarse y crear un Virtual Device.

como es la primera ves que ejecutamos el programa nos saldrá una ventana diciéndonos que si queremos crear un nuevo dispositivo, le damos que si, en la parte de abajo esta la opción para loguearse, se loguean si no lo han hecho, y crean un dispositivo dependiendo de sus requerimientos en mi caso cree un nexus 4 con android 5.0.1 ya que cuento con una maquina de 4 GB de ram para que no note que es lento. lo seleccionan y le dan en siguiente para continuar el proceso, si todo esta bien dale en aceptar y espera a que termine el proceso, esto dependerá de tu maquina para que se cree el dispositivo, al terminar el proceso dale en finish. Y listo ya esta corriendo el dispositivo android. 

Correr la app hecha en el emulador que creamos
------------------

Al ver que se esta ejecutando el dispositivo creado no lo cierres, ahora ve a la consola donde creamos el proyecto ionic que hicimos arriba, y estando en la carpeta raíz de la app ejecuta lo mismo como si lo fueras a emular en el teléfono físico con 

```
ionic run android
```
Deben recordar que si hicieron los pasos anteriores ya deberían de haber agregado la plataforma de android para ejecutar la app en el emulador de genymotion. 

Bueno esto fue todo, espero que les funcione como ami, he recogido cada paso que fui buscando para compartirlo ya que hay cosas que están en ingles.

> **Error al correr el comando ionic run android:**

> - FAILURE: Build failed with an exception.
     *What went wrong:
Execution failed for task ':CordovaLib:processDebugResources'.
> A problem occurred starting process 'command '/home/parada/android/android-sdk-linux/build-tools/23.0.2/aapt''
	*Try:
Run with --stacktrace option to get the stack trace. Run with --info or --debug option to get more log output.

Se debe instalar estos paquetes 

    sudo apt-get install lib32stdc++6
	sudo apt-get install lib32z1
	
	
> Solucionar problema de lentitud en el emulador Genymotion

 1. Open VirtualBox
 2. Right-click your Genymotion virtual device and select Settings
 3. Select the System section
 4. Check that the Acceleration tab is not dimmed and that the VT-x/AMD-v and Nested Paging options are enabled
 5. If not: try enabling virtualization in your PC's BIOS.